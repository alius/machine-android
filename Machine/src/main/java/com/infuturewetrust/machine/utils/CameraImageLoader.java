package com.infuturewetrust.machine.utils;

import android.content.Context;

public class CameraImageLoader {

    public static int getImageResource(Context context, String type) {
        return context.getResources().getIdentifier("cctv_" + type, "drawable", context.getPackageName());
    }
}
