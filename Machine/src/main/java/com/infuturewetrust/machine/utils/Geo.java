package com.infuturewetrust.machine.utils;


public class Geo {
    private static final double DEG_RAD_MUL = Math.PI/180;
    private static final double RAD_DEG_MUL = 180/Math.PI;
    private static final double EARTH_RADIUS = 6371.009;

    /**
     * Calculates the distance in KM between 2 points
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @return
     */
    public static double haversine(double lat1,
                                   double lon1,
                                   double lat2,
                                   double lon2) {
        double dLat = (lat2 - lat1) * DEG_RAD_MUL;
        double dLon = (lon2 - lon1) * DEG_RAD_MUL;
        lat1 *= DEG_RAD_MUL;
        lon1 *= DEG_RAD_MUL;
        lat2 *= DEG_RAD_MUL;
        lon2 *= DEG_RAD_MUL;
        double a = Math.pow(Math.sin(dLat / 2), 2) +
                Math.pow(Math.sin(dLon / 2), 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return EARTH_RADIUS * c;
    }
}
