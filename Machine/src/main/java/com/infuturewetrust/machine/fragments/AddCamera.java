package com.infuturewetrust.machine.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.infuturewetrust.machine.R;
import com.infuturewetrust.machine.models.Camera;
import com.infuturewetrust.machine.models.LatLngLocation;
import com.infuturewetrust.machine.utils.CameraImageLoader;

import java.util.List;

public class AddCamera extends Fragment implements AdapterView.OnItemSelectedListener {

    private static final String TAG = AddCamera.class.getName();

    private List<String> mCameraTypes;
    private LatLngLocation mLocation;
    private ImageView mCameraIcon;
    private SensorManager mSensorManager;
    private OnCameraAddListener mListener;
    private static boolean canUpdateBearing = true;
    private float[] mValues = new float[3];
    private float[] aValues = new float[3];

    private Spinner mSpinner;
    private EditText mStreet, mZip, mCity, mCountry,
            mDescription, mBearing, mLatitude, mLongitude;

    public interface OnCameraAddListener {
        public void onCameraAdd(Camera c);
    }

    public AddCamera(List<String> cameraTypes, LatLngLocation location) {
        mCameraTypes = cameraTypes;
        mLocation = location;
    }

    @Override
    public void onCreate(Bundle inState) {
        super.onCreate(inState);
        mSensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
    }

    @Override
    public void onResume() {
        super.onResume();
        canUpdateBearing = true;
        Sensor magField = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        Sensor accel = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(sensorEventListener, accel, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(sensorEventListener, magField, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onStop() {
        super.onStop();
        mSensorManager.unregisterListener(sensorEventListener);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnCameraAddListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnCameraAddListener");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_camera, container, false);
        mSpinner = (Spinner) v.findViewById(R.id.add_camera_type);
        final ArrayAdapter<CharSequence> adapter =
                new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, mCameraTypes);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        mSpinner.setOnItemSelectedListener(this);

        mLatitude = (EditText) v.findViewById(R.id.add_camera_latitude);
        mLatitude.setText(String.valueOf(mLocation.latitude));
        mLongitude = (EditText) v.findViewById(R.id.add_camera_longitude);
        mLongitude.setText(String.valueOf(mLocation.longitude));
        mStreet = (EditText) v.findViewById(R.id.add_camera_street);
        mZip = (EditText) v.findViewById(R.id.add_camera_zip);
        mCity = (EditText) v.findViewById(R.id.add_camera_city);
        mCountry = (EditText) v.findViewById(R.id.add_camera_country);
        mDescription = (EditText) v.findViewById(R.id.add_camera_description);
        mBearing = (EditText) v.findViewById(R.id.add_camera_bearing);
        mBearing.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                canUpdateBearing = false;
                return false;
            }
        });

        mCameraIcon = (ImageView) v.findViewById(R.id.add_camera_image);

        v.findViewById(R.id.add_camera_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Camera c = new Camera();
                    c.latitude = Double.parseDouble(mLatitude.getText().toString());
                    c.longitude = Double.parseDouble(mLongitude.getText().toString());
                    c.bearing = Double.parseDouble(mBearing.getText().toString());
                    c.type = mSpinner.getSelectedItem().toString();
                    c.street = mStreet.getText().toString();
                    c.zip = mZip.getText().toString();
                    c.city = mCity.getText().toString();
                    c.country = mCountry.getText().toString();
                    c.description = mDescription.getText().toString();
                    mListener.onCameraAdd(c);
                } catch (NullPointerException e) {
                    Toast.makeText(getActivity(), "Error adding new camera", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "NPE", e);
                }
            }
        });

        return v;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
        String type = parent.getItemAtPosition(pos).toString().toLowerCase();
        int id = CameraImageLoader.getImageResource(getActivity(), type);
        if (id > 0) {
            mCameraIcon.setImageResource(id);
        } else {
            mCameraIcon.setImageResource(R.drawable.cctv_generic);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private final SensorEventListener sensorEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            if (sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                mValues = sensorEvent.values;
            }

            if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                aValues = sensorEvent.values;
            }

            if (canUpdateBearing) {
                updateOrientation(calculateOrientation());
            }
        }

        @Override public void onAccuracyChanged(Sensor sensor, int i) {}
    };

    private float[] calculateOrientation() {
        float[] values = new float[3];
        float[] R = new float[9];
        float[] outR = new float[9];

        SensorManager.getRotationMatrix(R, null, aValues, mValues);
        SensorManager.remapCoordinateSystem(R, SensorManager.AXIS_X, SensorManager.AXIS_Z, outR);
        SensorManager.getOrientation(outR, values);

        // convert from radians to degrees
        values[0] = (float) Math.toDegrees(values[0]);
        values[1] = (float) Math.toDegrees(values[1]);
        values[2] = (float) Math.toDegrees(values[2]);

        return values;
    }

    private void updateOrientation(float[] value) {
        mBearing.setText(String.valueOf(value[0]));
    }
}
