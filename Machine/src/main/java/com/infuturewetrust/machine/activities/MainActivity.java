package com.infuturewetrust.machine.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.infuturewetrust.machine.R;
import com.infuturewetrust.machine.api.CameraService;
import com.infuturewetrust.machine.fragments.AddCamera;
import com.infuturewetrust.machine.models.Camera;
import com.infuturewetrust.machine.models.LatLngLocation;
import com.infuturewetrust.machine.utils.Geo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends Activity implements AddCamera.OnCameraAddListener {

    private GoogleMap mGoogleMap;
    private ProgressDialog mProgress;
    private List<String> mCameraTypes;
    private CameraService mCameraService;
    private SparseArray<Marker> mOverlay;
    private Map<String, Bitmap> mCameraIcons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ActionBar actionBar = getActionBar();
        if (actionBar != null) actionBar.setDisplayShowTitleEnabled(true);

        final MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null)
            mGoogleMap = mapFragment.getMap();

        if (mGoogleMap != null) {
            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.getUiSettings().setCompassEnabled(false);
            mGoogleMap.setOnMapClickListener(onMapClickListener);
        }

        final RestAdapter restAdapter = new RestAdapter.Builder()
                .setServer("http://machine.al1us.net").build();
        mCameraService = restAdapter.create(CameraService.class);
        mCameraService.getCameraTypes(onCameraTypesReceived);

        mProgress = new ProgressDialog(this);
        mOverlay = new SparseArray<Marker>();
        mCameraIcons = new HashMap<String, Bitmap>();
        mCameraIcons.put(Camera.Type.GENERIC, BitmapFactory.decodeResource(getResources(), R.drawable.camera));

        findViewById(R.id.map_ic_camera)
        .setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 if (mGoogleMap != null) {
                     LatLngBounds bounds = mGoogleMap.getProjection().getVisibleRegion().latLngBounds;
                     int distance = (int) (Geo.haversine(bounds.northeast.latitude,
                             bounds.northeast.longitude,
                             bounds.southwest.latitude,
                             bounds.southwest.longitude) * 1000);
                     if (distance > 1500) distance = 1500;
                     LatLng center = mGoogleMap.getCameraPosition().target;
                     mCameraService.getCameras(center.latitude, center.longitude, distance, onCamerasReceived);
                     mProgress.show();
                 }
             }
         });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private GoogleMap.OnMapClickListener onMapClickListener = new GoogleMap.OnMapClickListener() {
        @Override
        public void onMapClick(LatLng latLng) {
            LatLngLocation location = new LatLngLocation();
            location.latitude = latLng.latitude;
            location.longitude = latLng.longitude;
            AddCamera dialog = new AddCamera(mCameraTypes, location);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.map_container, dialog);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    };

    private Callback<List<String>> onCameraTypesReceived = new Callback<List<String>>() {
        @Override
        public void success(List<String> strings, Response response) {
            mCameraTypes = new ArrayList<String>();
            for (String type: strings) {
                mCameraTypes.add(type);
            }
        }

        @Override
        public void failure(RetrofitError retrofitError) {
            mCameraTypes = new ArrayList<String>();
        }
    };

    private Callback<List<Camera>> onCamerasReceived = new Callback<List<Camera>>() {
        @Override
        public void success(List<Camera> cameras, Response response) {
            if (mProgress.isShowing()) mProgress.cancel();

            for (Camera c: cameras) {
                if (mOverlay.get(c.hashCode(), null) == null) {
                    Log.d("CAMERAS", "ll: " + c.latitude + "," + c.longitude);
                    Bitmap icon = mCameraIcons.get(c.type);
                    if (icon == null) {
                        icon = mCameraIcons.get(Camera.Type.GENERIC);
                    }

                    mOverlay.put(c.hashCode(), mGoogleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(c.latitude, c.longitude))
                        .title(c.type)
                        .icon(BitmapDescriptorFactory.fromBitmap(icon))));
                }
            }
        }

        @Override
        public void failure(RetrofitError retrofitError) {
            if (mProgress.isShowing()) mProgress.cancel();
            Log.d("CAMERAS", "failed to receive: " + retrofitError.getResponse().toString());
        }
    };

    public Callback<String> onCameraAdded = new Callback<String>() {
        @Override
        public void success(String s, Response response) {
            Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void failure(RetrofitError retrofitError) {
            Toast.makeText(MainActivity.this, retrofitError.getBody().toString(), Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onCameraAdd(Camera c) {
        mCameraService.addCamera(c.latitude, c.longitude, c.type,
                c.description, c.bearing, c.city, c.zip, c.street, c.country, onCameraAdded);
        getFragmentManager().popBackStack();
    }
}
