package com.infuturewetrust.machine.api;

import com.infuturewetrust.machine.models.Camera;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface CameraService {
    @GET("/cameras/{latitude}/{longitude}/{radius}")
    void getCameras(
            @Path("latitude") double latitude,
            @Path("longitude") double longitude,
            @Path("radius") int radius,
            Callback<List<Camera>> cb);

    @GET("/camera/types")
    void getCameraTypes(Callback<List<String>> cb);

    @FormUrlEncoded
    @POST("/camera")
    void addCamera(
            @Field("latitude") double latitude,
            @Field("longitude") double longitude,
            @Field("type") String type,
            @Field("description") String description,
            @Field("bearing") double bearing,
            @Field("city") String city,
            @Field("zip") String zip,
            @Field("street") String street,
            @Field("country") String country,
            Callback<String> cb);
}
