package com.infuturewetrust.machine.models;

public class LatLngLocation {
    public double latitude;
    public double longitude;
}
