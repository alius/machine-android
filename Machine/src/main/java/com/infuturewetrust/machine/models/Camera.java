package com.infuturewetrust.machine.models;

public class Camera {
    public String street;
    public String description;
    public String zip;
    public String city;
    public String country;
    public String type;
    public double bearing;
    public double latitude;
    public double longitude;

    @Override
    public int hashCode() {
        StringBuilder sb = new StringBuilder();
        sb.append(latitude).append(longitude).append(type);
        return sb.toString().hashCode();
    }

    public static class Type {
        public static final String GENERIC = "generic";
    }
}
